title: Goldman Sachs
cover_image: '/images/blogimages/Goldman_Sachs_case_study.jpg'
cover_title: |
  Goldman Sachs improves from two daily builds to over a thousand per day
cover_description: |
  Engineering teams removed toolchain complexity and accelerated DevOps adoption through GitLab's automation
twitter_image: '/images/blogimages/Goldman_Sachs_case_study.jpg'

customer_logo: 'images/case_study_logos/GSsignature_Blue.png'
customer_logo_css_class: brand-logo-tall
customer_industry: Financial Services
customer_location: United States
customer_employees: 35,000+
customer_overview: |
  Goldman Sachs Technology Division and global strategists groups are at the critical center of their business. The dynamic environment at Goldman Sachs requires innovative strategic thinking and immediate, real-time solutions.
customer_challenge: |
  Goldman Sachs was looking to increase developer efficiency and software quality, achieve faster development cycles, and enable concurrent development activities.

key_benefits:
  - |
    Supporting an exponentially larger number of builds than previously thought possible
  - |
    Simplified Workflow - GitLab’s single application allows developers to manage all efforts in one UI.
  - |
    Simplified administration using one centralized instance for all repositories

customer_stats:
  - stat: 1000+
    label: CI feature branch builds a day by some teams
  - stat: 52k
    label: test cases across 11k test classes
  - stat: 1500+
    label: adopters in the first two weeks

customer_study_content:
  - title: the customer
    subtitle: Goldman Sachs solves clients engineering challenges
    content:
      - |
        Goldman Sachs is achieving what was previously assumed to be impossible. With the help of GitLab,
        the engineering team is now self-serving thousands of daily builds and dozens of teams are moving
        to production on a daily basis. Goldman Sachs empowered users with a strategic development tool and
        removed barriers around tool confusion and communication to speed up the delivery cycle.
      - |
        The Goldman Sachs engineering group is part of the technology division and global strategy groups which are the
        critical center of the financial service provider’s business. They solve the most challenging and pressing
        engineering problems for Goldman Sachs clients. The group builds massively scalable software and systems, architects
        low latency infrastructure solutions, proactively guards against cyber threats, and leverages machine learning
        alongside financial engineering to continuously turn data into action.
      - |
        Goldman Sachs dynamic environment requires innovative strategic thinking and immediate, real solutions.
        The group was looking to increase developer efficiency and software quality through faster development cycles
        and decreasing the time spent from feature design to production rollout. They wanted to accomplish all of this
        while enabling concurrent development activities.


  - title: the challenge
    subtitle: Increasing build speed by demolishing toolchain complexity
    content:
      - |
        The firm had built its own toolchain but was looking for a solution to increase concurrent development. They wanted a modern
        toolset for managing code that developers coming into the firm would likely already be familiar with; this led to an evaluation
        of GitLab and other git-based vendor products.
      - |
        From their discovery process, they decided GitLab had the best CI/CD infrastructure to meet their needs. Initial users reported
        that it was easy to use and offered a complete end-to-end platform for software development allowing them to replace their current
        toolchain and increase speed and coordination in the process.



  - title: the solution
    subtitle: GitLab increases Goldman Sachs build velocity
    content:
      - |
        GitLab is used as a complete ecosystem for development, source code control and reviews, builds,
        testing, QA, and production deployments. All the new strategic pieces of Goldman Sachs’ software
        development platforms are tied into GitLab.
      - |
        GitLab is helping the business-line engineering teams move faster to develop and improve their software
        and deliver for clients. One of the firm’s most important projects has moved from a release cycle of once
        every 1-2 weeks to once every few minutes because of GitLab.

  - blockquote: GitLab has allowed us to dramatically increase the velocity of development in our
        Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively
        bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs.
        We now see some teams running and merging 1000+ CI feature branch builds a day!
    attribution: Andrew Knight
    attribution_title: Managing Director at Goldman Sachs

  - content:
      - |
        GitLab provided them a single application to roll out DevOps to the thousands of users at the bank. The ease of
        people only having to learn one application accelerated their adoption time.  They were concerned that multiple tools,
        like most companies use, would be confusing, difficult to maintain and would deter adoption.



  - title:  the results
    subtitle: Dozens of teams pushing to production in less than 24 hours
    content:
      - |
        The engineering group at Goldman Sachs is achieving this increased efficiency and speed by collaborating and connecting on a new level.
        They are seeing dozens of teams pushing to production in less than 24 hours. By tying together all the different parts of the development
        lifecycle into one consistent ecosystem, GitLab has helped developers work more effectively. All the different parts of what they need to
        do can be brought together under a single umbrella to be more efficient.
      - |
        The team is also using GitLab as a means of moving teams to more strategic infrastructure which supports containers, as well as other
        strategic initiatives that are only offered on the new platform. This really helps make sure that the group is focusing efforts to use
        the latest technologies in the right places, and not just retrofit to the legacy infrastructure. GitLab also works well with Kubernetes
        out of the box which simplified their process.

  - blockquote: We’re bringing into the firm a platform that our engineers actually want to use – which helps drive adoption across multiple teams
        and increase productivity without having to ‘force’ anyone to adopt it. This is really helping to create an ecosystem where our end users are
        actively helping us drive towards our strategic goals - more releases, better controls, better software.
    attribution: George Grant
    attribution_title: VP technology fellow, Goldman Sachs
