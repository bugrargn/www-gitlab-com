---
layout: markdown_page
title: "FY20-Q4 OKRs"
---

This fiscal quarter will run from November 1, 2019 to January 31, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO: IACV

### 2. CEO: Popular next generation product

1. VP of Product Strategy: Get strategic thinking into the org. 10 [strategy reviews](https://gitlab.com/gitlab-com/Product/issues/379) for stages, 2[section strategy reviews](https://gitlab.com/gitlab-com/Product/issues/381) (Secure, Enablement).
1. VP of Product Strategy: Get acquisitions into shape; build a well-oiled machine. Build [soft-landing identification software](https://gitlab.com/gitlab-com/corporate-development/issues/1), identify 100 [qualified acquisition targets](/handbook/acquisitions/performance-indicators/#qualified-acquisition-targets).
1. VP of Product Strategy: Lay groundwork for strategic initiatives. Research 5 [strategic initiatives](https://docs.google.com/document/d/1CBvC5iSgvSfn0oBAu3ph9MAzrMHxhKCSjvigz0Lm1VM/edit).

### 3. CEO: Great team
