(function() {
  var mobileIcon = document.getElementById('mobile-icon');
  var mobileMenu = document.getElementById('mobile-menu');
  var exitIcon = document.getElementById('exit-icon');
  var mobileMenuOpen = false;

  function toggleMobileMenu() {
    mobileMenuOpen = !mobileMenuOpen;
    if (mobileMenuOpen) {
      mobileMenu.classList.remove('display-none');
      TweenMax.staggerTo('.mobile-menu ul li', 0.6, {ease: Power4.easeOut, y: 0, opacity: 1}, 0.1);
    } else {
      mobileMenu.classList.add('display-none')
      TweenMax.staggerTo('.mobile-menu ul li', 0.6, {ease: Power4.easeOut, y: 40, opacity: 0}, 0.1);
    }
  }

  mobileIcon.addEventListener('click', toggleMobileMenu);
  exitIcon.addEventListener('click', toggleMobileMenu);
})();
