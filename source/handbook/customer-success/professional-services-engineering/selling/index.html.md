---
layout: markdown_page
title: "Selling Professional Services"
---
# Selling Professional Services
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

## Value sell for Professional Services

### Elevator Pitch

**The Problem - Customer Perspective**

The customer has decided that GitLab is their tool of choice going forward, but has concerns about realizing the value of GitLab quickly and seamlessly.  These concerns can be in various areas:

* Lack of subject matter expertise on the GitLab technology stack
* Lack of adequate resources to promptly stand up GitLab
* Lack of sufficient resources to stand up GitLab in a highly-available or geographically dispersed way
* Concern around a large number of users coming off of legacy systems
* Concerns about change management for new processes
* Lack of confidence in the ability to deliver on the transformative promise of adopting "all" of GitLab

**Solution**

### Single Sentence

GitLab Professional Services helps organizations reduce time to market by accelerating the adoption of modern software delivery methods.

### Short Message

"The whole is greater than the sum of its parts" - and this is particularly true in modern software development. GitLab enables all aspects intrinsic to software delivery, and we provide specialized training in these practices, such as CI/CD, version control, metrics, and more.

### Long Message

Adopting GitLab gives you the best-of-breed Concurrent DevOps tool on the market while Professional Services will help you also align your people and processes to match.

Our Professional Services team is made up of not only GitLab subject matter experts but seasoned DevOps professionals who have experience in deploying and maintaining both large-scale applications as well as creating and teaching best practices throughout the SDLC.  Our experts help lead Concurrent DevOps Transformations, providing direct support to our customer’s strategic business initiatives.  Their experience with other Fortune 500 enterprises allows you to crowd-source your enterprise’s digital transformation.

GitLab's Professional Services team exists to enable you to realize the full value of your GitLab installation.  We can provide direct implementation support to ensure your GitLab installation is resilient and secure.  We also offer migration services to facilitate your transition to GitLab by delivering a clean dataset to resume operations at once.   Our education and specialized training provide training in best practices, such as CI/CD, version control, metrics, and more.

## Selling services workflow
There are three different ways that services can be sold: 

1. Through the use of a [SKU](/handbook/customer-success/professional-services-engineering/#professional-services-skus) which come with a boilerplate description and fixed cost
1. Through a Standard SOW which is created by the [Services Calculator](http://services-calculator.gitlab.io/)
1. Through a Custom SOW following the process below:

### Custom SOW Process
1. Sales to introduce early in discussions
1. The SA (or TAM where applicable) can do basic scoping and use [calculator](/handbook/customer-success/professional-services-engineering/selling/#services-calculator) for an estimate
  - This should be a good estimate to secure budget
  - Customer should execute Subscription Agreement AND Consulting Services Agreement
1. The SA or TAM can use the [custom SOW scoping details](/handbook/customer-success/professional-services-engineering/scoping/) page to help drive the conversation and uncover required capabilities for the custom SOW.
1. The SA or TAM will create the SoW from the [template](https://docs.google.com/document/d/1X8_EiX8kgJdpaVlydbTJg5pn4RXeDvYOIyok2G1A69I/edit), scoping the project and estimating both schedule and cost for the SoW. This [deck](https://drive.google.com/open?id=1ro9wlLHsoOMC-iYJpxy_RTCD4PfjDFZEdbuMyDD6WOk), accessible by GitLab employees only, provides detailed guidance on SoW creation.
1. If customer/prospect is ready to consume services OR the ask is too complicated for the calculator
  - Take a best estimate approach based on deliverables and previously executed SoW examples
  - Discuss with the Solution Architects Manager
  - SA (or TAM) may [open an Issue in the Professional Services project](/handbook/customer-success/professional-services-engineering/#professional-service-engineering-issue-board) to request a Professional Services Engineer (PSE)
1. PSE conducts more detailed scoping call (only when needed) to [prepare SoW](/handbook/customer-success/professional-services-engineering/#statement-of-work-creation)
1. If there are additional scoping questions needed to scope the engagement, Professional Services Engineering will inform the account team within three (3) business days of this call.
1. Once all scoping questions are complete, the SOW will be completed and ready for the account team review within in one (1) business week.
1. An issue is created in the [Professional Services project](/handbook/customer-success/professional-services-engineering/#professional-service-engineering-issue-board) where the SoW is linked. The SoW requires approval by the Professional Services Manager and/or the Director of Customer Success.
1. SoW will be assigned and typically started within 4 weeks lead time.
1. [SoW delivered and executed](/handbook/customer-success/professional-services-engineering/workflows/)
1. Services fulfilled

## Sales Collateral 

### Pitch Deck

To discuss our services offerings with prospects, it is often helpful to have a few slides to describe the role of the professional services team.  Feel free to use this deck directly - however if you'd like to modify it please first make a copy.

[Professional Services Pitch Deck](http://bit.ly/psslides)

### Data Sheets

Professional Services Data Sheets are available as subpages to the marketing site. You can find them through the [Professional Services portal](/services/).

## Services Calculator

The goal of the services calculator is to provide the sales team with the ability to self-serve and create a ROM package for a customer with the minimal amount of information.  You can get access to the service calculator [here](https://services-calculator.gitlab.io/).

## Other Collateral

For all other sales collateral documentation, see the [Professional Services Sales Enabelment folder](https://drive.google.com/drive/u/0/folders/1vLhSdmlwClou_16I1SU9d3X0oG1EtBHv) on Google Drive.

## FAQ

#### Do we have set SKUs I can use?

Yes - for off the shelf items, we have [SKUs](/handbook/customer-success/professional-services-engineering/#professional-services-skus).

#### What are our daily or hourly rates?

We do not currently have an hourly or daily rate.  Nor do we plan to have an hourly rate.  Just as with GitLab support, the mission of our Professional Service group is not to bill hours, but to achieve success for our Customers.  So just as we don't offer support by the call or by the hour, we do not offer Professional Services by the day or the hour.

In the future, we may have a daily rate or a daily rate for on-site support.  However, we currently do not for the same reasons listed above.

#### What if the customer only wants training?

If the customer is an EE customer, we can offer training.  However, training will need to be scoped out by the customer success department before quoting a price.  The account executive will also be required to provide the use case for the need for just training.  Example use case might be: Customer is under license utilization and we need to prevent churn, help expand usage into additional groups and other business units.

#### What options exist for CE Customers?

GitLab CE is ideal for personal projects or small groups with minimal user management and workflow control needs.  Because these groups don't typically need a lot of focus on scaled implementation or training, we currently do not offer implementation, integration or training services to our CE customers.  Many of our [partners](/resellers/) offer such services.  However, we find that many customers who have been running a CE instance and are looking to transition to a scaled implementation of GitLab may require a [Discovery Engagement](#discovery-engagement) to determine the customer's long-term needs.

If a customer is upgrading from CE to EE, Professional Services should be engaged to scope out the requirements for the transition if the customer will need services in the transition.
