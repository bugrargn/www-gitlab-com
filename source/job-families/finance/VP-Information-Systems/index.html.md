---
layout: job_family_page
title: "VP, Information Systems"
---


## Responsibilities
1. Strategic and operational ownership for systems, business processes and data company wide.
1. Lead IT strategic and operational planning to achieve business goals by fostering innovation,
prioritizing IT initiatives, and coordinating the evaluation, deployment, and management of current
and future IT systems across the organization to ensure flexible and effective systems and business processes.
1. Identify opportunities for appropriate and cost-effective investment in information systems.
1. Evaluate and make recommendations on staffing, outsourcing, purchasing, and in-house development of systems and processes.
1. Responsible for information systems budget oversight and cost control.
1. Work in partnership with business functions to build a strategy and roadmap that will support future growth of the company.
1. Participate in and facilitate the strategic and operational process improvement of the organization.
1. Coordinate project teams throughout the organization as it relates to Information Systems.
1. Identify potential bottlenecks and prepare the company for hyper-growth.

## Requirements
1. 

## Performance Indicators
1. 



## Hiring Process
Candidates for this position can expect the hiring process to follow the order below.
1. Screening call with a team member from our recruiting team.
1. Interview with Director of Business Operations
1. Interview with 2+ Business Operations Department members
1. Interview with 1-2 executive team members
1. Interview with CEO plus 1-2 board members

Please note that a candidate may declined from the position at any stage of the process. 
Additional details about our process can be found on our [hiring page](/handbook/hiring). 
