---
layout: markdown_page
title: "Category Vision - Unit Testing"
---

- TOC
{:toc}

## Unit Testing

Unit testing ensures that individual components built within a pipeline perform as expected, and are an important part of a Continuous Integration framework.

- [Issue List](hhttps://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AUnit%20Testing)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/TBD) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why
We would like to help developers validate their code quickly through unit tests and when those tests fail provide the data and context they need within GitLab to diagnose a broken pipeline and move on quickly.

To further that vision the next item we work on will be [gitlab#23257](https://gitlab.com/gitlab-org/gitlab/issues/23257).


## Maturity Plan

This category is currently at the "Minimal" maturity level, and our next maturity target is "Viable" (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).

## Competitive Landscape

TBD

## Top Customer Success/Sales Issue(s)

TBD

## Top Customer Issue(s)

The most popular issue is [gitlab#23257](https://gitlab.com/gitlab-org/gitlab/issues/23257) which builds on the previously released [JUnit XML Test Summary in MR Widget](https://gitlab.com/gitlab-org/gitlab-foss/issues/45318).

## Top Internal Customer Issue(s)

The QA department has opened an interesting issue [gitlab#14954](https://gitlab.com/gitlab-org/gitlab/issues/14954), aimed at solving a problem where they have have limited visibility into long test run times that can impact efficiency.

## Top Vision Item(s)

TBD

